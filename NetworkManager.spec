%global wireless_tools_version 1:28-0pre9
%global wpa_supplicant_version 1:1.1
%global ppp_version %(pkg-config --modeversion pppd 2>/dev/null || sed -n 's/^#define\\s*VERSION\\s*"\\([^\\s]*\\)"$/\\1/p' %{_includedir}/pppd/patchlevel.h 2>/dev/null | grep . || echo bad)
%global glib2_version %(pkg-config --modversion glib-2.0 2>/dev/null || echo bad)
%global real_version 1.42.8
%global snapshot %{nil}
%global git_sha %{nil}
%global obsoletes_device_plugins 1:0.9.9.95-1
%global obsoletes_ppp_plugin     1:1.5.3
%global systemd_dir %{_prefix}/lib/systemd/system

%if "x%{?snapshot}" != "x"
%global snapshot_dot .%{snapshot}
%endif
%if "x%{?git_sha}" != "x"
%global git_sha_dot .%{git_sha}
%endif

%global snap %{?snapshot_dot}%{?git_sha_dot}
%global real_version_major %(printf '%s' '%{real_version}' | sed -n 's/^\\([1-9][0-9]*\\.[1-9][0-9]*\\)\\.[0-9][0-9]*$/\\1/p')
%global systemd_units NetworkManager.service NetworkManager-wait-online.service NetworkManager-dispatcher.service nm-priv-helper.service
%global systemd_units_cloud_setup nm-cloud-setup.service nm-cloud-setup.timer

%bcond_without adsl
%bcond_without bluetooth
%bcond_without wwan
%bcond_without team
%bcond_without wifi
%bcond_with iwd
%bcond_without ovs
%bcond_without ppp
%bcond_without nmtui
%bcond_without regen_docs
%bcond_with    debug
%bcond_with    test
%bcond_with    lto
%bcond_with    sanitizer
%bcond_without crypto_gnutls
%bcond_without nm_cloud_setup
%bcond_with    firewalld_zone

%global dbus_version 1.1
%global dbus_sys_dir %{_datadir}/dbus-1/system.d
%if %{with bluetooth} || %{with wwan}
%global with_modem_manager_1 1
%else
%global with_modem_manager_1 0
%endif
%global dhcp_default dhclient

%define _lto_cflags %{nil}

Name:             NetworkManager
Version:          1.44.2
Epoch:            1
Release:          3
Summary:          Network connection manager and user applications
License:          GPL-2.0-or-later
URL:              https://networkmanager.dev/
Source:           https://download.gnome.org/sources/NetworkManager/%{real_version_major}/%{name}-%{version}.tar.xz
Source1:          NetworkManager.conf
Source2:          00-server.conf
Patch1:           fix-wants-and-add-requires.patch
Patch2:           bugfix-use-PartOf-replace-Requires-in-service.patch
Patch3:           bugfix-recover-to-30s-timeout-in-NetworkManager-wait-online.patch
Patch4:           NetworkManager-Add-sw64-architecture.patch
Patch5:           delete-lease-file-when-connection-deleted.patch
Patch6:           0001-add-NM-to-support-wifi6.patch

Patch6000:        backport-lldp-fix-crash-dereferencing-NULL-pointer-during-deb.patch
Patch6001:        backport-lldp-fix-multiple-access-to-argument-in-logging-macr.patch

BuildRequires:    gcc libtool pkgconfig automake autoconf intltool gettext-devel ppp-devel gnutls-devel
BuildRequires:    dbus-devel glib2-devel gobject-introspection-devel jansson-devel
BuildRequires:    dhclient readline-devel audit-libs-devel gtk-doc libudev-devel libuuid-devel /usr/bin/valac polkit-devel
BuildRequires:    iptables libxslt systemd systemd-devel libcurl-devel libndp-devel python3-gobject-base 
BuildRequires:    ModemManager-glib-devel newt-devel /usr/bin/dbus-launch python3 python3-dbus libselinux-devel chrpath
%if %{with team}
BuildRequires:    teamd-devel
%endif
%if %{with firewalld_zone}
BuildRequires:    firewalld-filesystem
%endif

%if %{with bluetooth}
BuildRequires:    bluez-libs-devel
%endif
Requires(post):   systemd
Requires(post):   /usr/sbin/update-alternatives
Requires(preun):  systemd
Requires(preun):  /usr/sbin/update-alternatives
Requires(postun): systemd
Requires:         dbus  glib2
Requires:         %{name}-libnm = %{epoch}:%{version}-%{release}
Obsoletes:        NetworkManager < %{obsoletes_device_plugins} NetworkManager < %{obsoletes_ppp_plugin}
Obsoletes:        dhcdbd NetworkManager < 1.0.0
Obsoletes:        NetworkManager-wimax < 1.2
Obsoletes:        NetworkManager-bt NetworkManager-atm
Conflicts:        NetworkManager-vpnc < 1:0.7.0.99-1 NetworkManager-openvpn < 1:0.7.0.99-1
Conflicts:        NetworkManager-pptp < 1:0.7.0.99-1 NetworkManager-openconnect < 0:0.7.0.99-1
Conflicts:        kde-plasma-networkmanagement < 1:0.9-0.49.20110527git.nm09

Provides:         %{name}-dispatcher-routing-rules
Obsoletes:        %{name}-dispatcher-routing-rules
Provides:         %{name}-config-routing-rules = %{epoch}:%{version}-%{release}
Obsoletes:        %{name}-config-routing-rules < %{epoch}:%{version}-%{release}
Provides:         %{name}-tui
Obsoletes:        %{name}-tui
Provides:         %{name}-adsl
Obsoletes:        %{name}-adsl
Provides:         %{name}-ovs
Obsoletes:        %{name}-ovs

%description
NetworkManager attempts to keep an active network connection available
at all times.  The point of NetworkManager is to make networking
configuration and setup as painless and automatic as possible.	If
using DHCP, NetworkManager is intended to replace default routes,
obtain IP addresses from a DHCP server, and change name servers
whenever it sees fit.

	
%if %{with nm_cloud_setup}
%package cloud-setup
Summary: Automatically configure NetworkManager in cloud
Requires: %{name} = %{epoch}:%{version}-%{release}
Requires: %{name}-libnm%{?_isa} = %{epoch}:%{version}-%{release}

%description cloud-setup
Installs a nm-cloud-setup tool that can automatically configure
NetworkManager in cloud setups. Currently only EC2 is supported.
This tool is still experimental.
%endif
%if %{with wifi}
%package wifi
Summary:          Wifi plugin for NetworkManager
Requires:         %{name}%{?_isa} = %{epoch}:%{version}-%{release}
Requires:         wpa_supplicant
Requires:         wireless-regdb

Obsoletes: NetworkManager < %{obsoletes_device_plugins}

%description wifi
This package contains NetworkManager support for Wifi devices.
%endif

%package          wwan
Summary:          Mobile broadband device plugin for %{name}
Requires:         %{name} = %{epoch}:%{version}-%{release}
Requires:         ModemManager
Obsoletes:        NetworkManager < %{obsoletes_device_plugins}

%description      wwan
This package contains NetworkManager support for mobile broadband (WWAN)
devices.

%if %{with bluetooth}
%package          bluetooth
Summary:          Bluetooth device plugin for %{name}
Requires:         %{name} = %{epoch}:%{version}-%{release}
Requires:         NetworkManager-wwan
Requires:         bluez
Obsoletes:        NetworkManager < %{obsoletes_device_plugins}

%description      bluetooth
This package contains NetworkManager support for Bluetooth device
%endif

%if %{with team}
%package team
Summary:Team device plugin for NetworkManager
Requires:%{name}%{?_isa} = %{epoch}:%{version}-%{release}
Obsoletes:NetworkManager < %{obsoletes_device_plugins}

%description team
This package contains NetworkManager support for team devices.
%endif
	
%if %{with ppp}
%package ppp
Summary:         PPP plugin for NetworkManager
Requires:        %{name} = %{epoch}:%{version}-%{release}
Obsoletes:       NetworkManager < %{obsoletes_ppp_plugin}
 
%description ppp
This package contains NetworkManager support for PPP.
%endif

%package          libnm
Summary:          Libraries for adding NetworkManager support to applications (new API).
Conflicts:        NetworkManager-glib < %{epoch}:%{version}-%{release}

%description      libnm
This package contains the libraries that add NetworkManager support to applications (new API).

%package          libnm-devel
Summary:          Header files and Development files for adding NetworkManager support to applications (new API).
Requires:         %{name}-libnm = %{epoch}:%{version}-%{release} pkgconfig glib2-devel
Requires: glib2-devel
Requires: pkgconfig

%description      libnm-devel
This package contains the header and development files  for
developing applications that use %{name}-libnm.

%package          config-server
Summary:          NetworkManager config file for "server-like" defaults
BuildArch:        noarch

%description      config-server
This adds a NetworkManager configuration file to make it behave more
like the old "network" service. In particular, it stops NetworkManager
from automatically running DHCP on unconfigured ethernet devices, and
allows connections with static IP addresses to be brought up even on
ethernet devices with no carrier.

%package_help

%prep
%autosetup -p1 -n NetworkManager-%{version}

%build
%if %{with regen_docs}
gtkdocize
%endif
autoreconf --install --force
#intltoolize --automake --copy --force
%configure \
	--disable-silent-rules \
	--with-dhclient=yes \
	--with-dhcpcd=no \
	--with-dhcpcanon=no \
	--with-config-dhcp-default=%{dhcp_default} \
%if %{with crypto_gnutls}
	--with-crypto=gnutls \
%else
	--with-crypto=nss \
%endif
%if %{with sanitizer}
	--with-address-sanitizer=exec \
%else
	--with-address-sanitizer=no \
	--disable-undefined-sanitizer \
%endif
%if %{with debug}
	--enable-more-logging \
	--with-more-asserts=10000 \
%else
	--disable-more-logging \
	--without-more-asserts \
%endif
	--enable-ld-gc \
%if %{with lto}
	--enable-lto \
%else
	--disable-lto \
%endif
	--with-libaudit=yes-disabled-by-default \
%if 0%{?with_modem_manager_1}
	--with-modem-manager-1=yes \
%else
	--with-modem-manager-1=no \
%endif
%if %{with wifi}
	--enable-wifi=yes \
	--with-wext=no \
%else
	--enable-wifi=no \
%endif
%if %{with iwd}
	--with-iwd=yes \
%else
	--with-iwd=no \
%endif
%if %{with nm_cloud_setup}
	--with-nm-cloud-setup=yes \
%else
	--with-nm-cloud-setup=no \
%endif
	--enable-vala=yes \
	--enable-introspection=yes \
%if %{with regen_docs}
	--enable-gtk-doc=yes \
%else
	--enable-gtk-doc=no \
%endif
%if %{with team}
	--enable-teamdctl=yes \
%else
	--enable-teamdctl=no \
%endif
%if %{with ovs}
	--enable-ovs=yes \
%else
	--enable-ovs=no \
%endif
	--with-selinux=yes \
	--enable-polkit=yes \
	--enable-polkit-agent \
	--enable-modify-system=yes \
	--enable-concheck=yes \
	--with-libpsl=no \
	--with-ebpf=no \
	--with-session-tracking=systemd \
	--with-suspend-resume=systemd \
	--with-systemdsystemunitdir=%{systemd_dir} \
	--with-system-ca-path=/etc/pki/tls/cert.pem \
	--with-dbus-sys-dir=%{dbus_sys_dir} \
	--with-tests=yes \
%if %{with test}
	--enable-more-warnings=error \
%else
	--enable-more-warnings=yes \
%endif
	--with-valgrind=no \
	--enable-ifcfg-rh=yes \
	--enable-ifupdown=no \
%if %{with ppp}
	--with-pppd-plugin-dir=%{_libdir}/pppd/%{ppp_version} \
	--enable-ppp=yes \
%endif
	--with-dist-version=%{version}-%{release} \
	--with-config-plugins-default='ifcfg-rh' \
	--with-config-dns-rc-manager-default=symlink \
	--with-config-logging-backend-default=journal \
	--enable-json-validation \
%if %{with firewalld_zone}
	--enable-firewalld-zone=yes \
%else
	--enable-firewalld-zone=no \
%endif
	--with-resolvconf=no \
	--with-netconfig=no \
%if %{with nm_cloud_setup}
	--with-nm-cloud_setup=yes
%else
	--with-nm-cloud_setup=no
%endif

%make_build

%install
make install DESTDIR=%{buildroot}
cp  %{SOURCE1} %{buildroot}%{_sysconfdir}/%{name}/
cp  %{SOURCE2} %{buildroot}%{_prefix}/lib/%{name}/conf.d/
cp examples/dispatcher/10-ifcfg-rh-routes.sh %{buildroot}%{_sysconfdir}/%{name}/dispatcher.d/
ln -s ../no-wait.d/10-ifcfg-rh-routes.sh %{buildroot}%{_sysconfdir}/%{name}/dispatcher.d/pre-up.d/
ln -s ../10-ifcfg-rh-routes.sh %{buildroot}%{_sysconfdir}/%{name}/dispatcher.d/no-wait.d/

%find_lang %{name}
chrpath -d %{buildroot}/%{_libdir}/%{name}/%{version}-%{release}/*.so*
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}/%{version}-%{release}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%delete_la_and_a
find %{buildroot}%{_datadir}/gtk-doc -exec touch --reference configure.ac '{}' \+
%if 0%{?__debug_package}
mkdir -p %{buildroot}%{_prefix}/src/debug/NetworkManager-%{real_version}
cp valgrind.suppressions %{buildroot}%{_prefix}/src/debug/NetworkManager-%{real_version}
%endif

touch %{buildroot}%{_sbindir}/ifup %{buildroot}%{_sbindir}/ifdown

%check
#To avoid some test failed which need root permission
NMTST_FORCE_REAL_ROOT=true %make_build check

%pre
if [ -f "%{systemd_dir}/network-online.target.wants/NetworkManager-wait-online.service" ] ; then
        systemctl enable NetworkManager-wait-online.service || :
	fi

%post
# skip triggering if udevd isn't even accessible, e.g. containers or
# rpm-ostree-based systems
if [ -S /run/udev/control ]; then
    /usr/bin/udevadm control --reload-rules || :
    /usr/bin/udevadm trigger --subsystem-match=net || :
fi
%if %{with firewalld_zone}
%firewalld_reload
%endif

%systemd_post %{systemd_units}


if [ -f %{_sbindir}/ifup -a ! -L %{_sbindir}/ifup ]; then
    /usr/sbin/update-alternatives --remove ifup %{_libexecdir}/nm-ifup >/dev/null 2>&1 || :
else
   /usr/sbin/update-alternatives --install %{_sbindir}/ifup ifup %{_libexecdir}/nm-ifup 50 \
        --slave %{_sbindir}/ifdown ifdown %{_libexecdir}/nm-ifdown
fi
/sbin/ldconfig

%if %{with nm_cloud_setup}
%post cloud-setup
%systemd_post %{systemd_units_cloud_setup}
%endif


%preun
if [ $1 -eq 0 ]; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable NetworkManager.service >/dev/null 2>&1 || :
    /usr/sbin/update-alternatives --remove ifup %{_libexecdir}/nm-ifup >/dev/null 2>&1 || :
fi
%systemd_preun NetworkManager-wait-online.service NetworkManager-dispatcher.service nm-priv-helper.service

%if %{with nm_cloud_setup}
%preun cloud-setup
%systemd_preun %{systemd_units_cloud_setup}
%endif


%postun
/usr/bin/udevadm control --reload-rules || :
/usr/bin/udevadm trigger --subsystem-match=net || :

%systemd_postun %{systemd_units}

%ldconfig_scriptlets glib
%ldconfig_scriptlets libnm
/sbin/ldconfig
%if %{with nm_cloud_setup}
%postun cloud-setup
%systemd_postun %{systemd_units_cloud_setup}
%endif

%files
%defattr(-,root,root)
%doc AUTHORS 
%license COPYING
%license COPYING.LGPL
%license COPYING.GFDL
%{_datadir}/doc/NetworkManager/examples/server.conf
%{_bindir}/nmcli
%{_bindir}/nm-online
%{_sbindir}/NetworkManager
%{_libexecdir}/nm-initrd-generator
%ghost %attr(755, root, root) %{_sbindir}/ifdown
%ghost %attr(755, root, root) %{_sbindir}/ifup
%dir %{_prefix}/lib/NetworkManager/conf.d
%dir %{_prefix}/lib/NetworkManager/VPN
%{systemd_dir}/NetworkManager.service
%{systemd_dir}/NetworkManager-wait-online.service
%{systemd_dir}/NetworkManager-dispatcher.service
%{systemd_dir}/nm-priv-helper.service
%{_prefix}/lib/udev/rules.d/*.rules
%{_libdir}/%{name}/%{version}-%{release}/libnm-settings-plugin-ifcfg-rh.so
%{_libexecdir}/nm-if*
%{_libexecdir}/nm-priv-helper
%{_libexecdir}/nm-dhcp-helper
%{_libexecdir}/nm-daemon-helper
%{_libexecdir}/nm-dispatcher
%{_datadir}/bash-completion/completions/nmcli
%{dbus_sys_dir}/*.conf
%{_datadir}/dbus-1/system-services/*.service
%{_datadir}/polkit-1/actions/*.policy
%dir %{_sysconfdir}/NetworkManager/conf.d
%dir %{_sysconfdir}/NetworkManager/dispatcher.d/pre-down.d
%dir %{_sysconfdir}/NetworkManager/dnsmasq.d
%dir %{_sysconfdir}/NetworkManager/dnsmasq-shared.d
%dir %{_sysconfdir}/NetworkManager/system-connections
%dir %{_sysconfdir}/sysconfig/network-scripts
%config(noreplace) %{_sysconfdir}/NetworkManager/NetworkManager.conf
%dir %{_localstatedir}/lib/%{name}
%{_sysconfdir}/%{name}/dispatcher.d/10-ifcfg-rh-routes.sh
%{_sysconfdir}/%{name}/dispatcher.d/no-wait.d/10-ifcfg-rh-routes.sh
%{_sysconfdir}/%{name}/dispatcher.d/pre-up.d/10-ifcfg-rh-routes.sh
%{_libdir}/pppd/%{ppp_version}/nm-pppd-plugin.so
%{_bindir}/nmtui*
%if %{with adsl}
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-adsl.so
%endif
%if %{with firewalld_zone}
%{_prefix}/lib/firewalld/zones/nm-shared.xml
%endif
%if %{with ovs}
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-ovs.so
%{systemd_dir}/NetworkManager.service.d/NetworkManager-ovs.conf
%endif
%config(noreplace) /etc/ld.so.conf.d/*

%if %{with nm_cloud_setup}
%files cloud-setup
%{_libexecdir}/nm-cloud-setup
%{systemd_dir}/nm-cloud-setup.service
%{systemd_dir}/nm-cloud-setup.timer
%{_prefix}/lib/%{name}/dispatcher.d/90-nm-cloud-setup.sh
%{_prefix}/lib/%{name}/dispatcher.d/no-wait.d/90-nm-cloud-setup.sh
%{_prefix}/lib/%{name}/dispatcher.d/pre-up.d/90-nm-cloud-setup.sh
%endif

%if %{with team}
%files team
%defattr(-,root,root)
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-team.so
%endif

%if %{with wwan}
%files wwan
%defattr(-,root,root)
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-wwan.so
%{_libdir}/%{name}/%{version}-%{release}/libnm-wwan.so
%endif

%if %{with bluetooth}
%files bluetooth
%defattr(-,root,root)
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-bluetooth.so
%endif

%if %{with wifi}
%files wifi
%defattr(-,root,root)
%{_libdir}/%{name}/%{version}-%{release}/libnm-device-plugin-wifi.so
%endif


%if %{with ppp}
%files ppp
%defattr(-,root,root)
%{_libdir}/%{name}/%{version}-%{release}/libnm-ppp-plugin.so
%endif
%files libnm -f %{name}.lang
%defattr(-,root,root)
%{_libdir}/libnm.so.0*
%{_libdir}/girepository-1.0/*.typelib

%files libnm-devel
%defattr(-,root,root)
%{_includedir}/libnm/*.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libnm.so
%{_datadir}/gir-1.0/NM-1.0.gir
%{_datadir}/vala/vapi/libnm*
%{_datadir}/dbus-1/interfaces/*.xml

%files config-server
%{_prefix}/lib/%{name}/conf.d/00-server.conf

%files help
%defattr(-,root,root)
%doc CONTRIBUTING.md NEWS TODO
%{_mandir}/man1/nmcli.1*
%{_mandir}/man1/nm-online.1*
%{_mandir}/man5/*.5*
%{_mandir}/man7/nmcli-examples.7*
%{_mandir}/man8/*.8*
%{_mandir}/man7/nm-openvswitch.7*
%{_mandir}/man1/nmtui*.1*
%{_datadir}/gtk-doc/html/libnm
%{_datadir}/gtk-doc/html/NetworkManager

%changelog
* Fri Jan 24 2025 Funda Wang <fundawang@yeah.net> - 1:1.44.2-3
- Type:CVE
- CVE:CVE-2024-6501
- SUG:NA
- DESC: fix CVE-2024-6501
- DESC: 1.44.2 does not build with lto for now

* Tue Apr 9 2024 liuzhilin <liuzhilin@uniontech.com> - 1:1.44.2-2
- add NM to support wifi6(fix dde-network-core build error)
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:Support wireless networks connecting WIFI6 encryption methods,
       so this demand is achieved in NetworkManager, KF5-NetworkManager-QT,
       DDE-Network-Core software package

* Mon Feb 5 2024 liubo <liubo335@huawei.com> - 1:1.44.2-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update to 1.44.2

* Tue Aug 29 2023 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.42.8-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:sync bugfix patch: delete lease file when connection deleted and 
       update the way to get ppp version because patchlevel.h no more provided in  ppp-2.5.0

* Thu Aug 24 2023 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.42.8-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:NetworkManager-dispatcher.service not enabled when installed due to systemd_units not defined

* Tue Aug 8 2023 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.42.8-2
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:sync,merge ovs into main package

* Sat Jul 29 2023 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.42.8-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update to 1.42.8

* Thu Mar 30 2023 Dongxing Wang <dxwangk@isoftstone.com> - 1:1.32.12-18
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:correct the info for nmcli device connect

* Mon Mar 20 2023 sunsuwan <sunsuwan3@huawei.com> - 1:1.32.12-17
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:delete lease file when connection deleted

* Wed Dec 7 2022 chendexi <chendexi@kylinos.cn> - 1:1.32.12-16
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:Modify invalid upstream address

* Tue Dec 6 2022 Chenxi Mao <chenxi.mao@suse.com> - 1:1.32.12-15
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: Set NMTST_FORCE_REAL_ROOT to true to skip test which need root user permission

* Fri Dec 2 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-14
- Type:feature
- CVE:NA
- SUG:NA
- DESC:Add sw64 architecture

* Tue Nov 15 2022 yuelg <yuelg@chinaunicom.cn> - 1:1.32.12-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix minor written mistake NetworkManaer

* Thu Sep 29 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:use multi-thread build

* Thu Sep 29 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete extra so file in main packet

* Mon Aug 1 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix release version

* Mon Aug 1 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:libnm:fix warning when setting wrong ethtool ternary value
core: reload config for active devices
libnm: fix crash on failure of nm_vpn_plugin_info_new_from_file()
libnm: fix crash in _nm_ip_route_validate_all for invalid-route

* Sat Jul 30 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix D-Bus policy file installed in /usr/share

* Thu Jul 28 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1:1.32.12-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix remove NetworkManager wrongly when yum remove NetworkManager-bluetooth subpackage that is not installed

* Wed Jul 27 2022 gaoxingwang <gaoxingwang1@huawei.com> - 1.32.12-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:recover to 30s timeout in NetworkManager-wait-online service

* Wed Mar 9 2022 chxssg <chxssg@qq.com> - 1.32.12-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix ipv6 external route miss

* Mon Mar 7 2022 seuzw <930zhaowei@163.com> - 1.32.12-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix the issue that NetworkManager service does not self-heal when the dbus service is abnormal

* Tue Jan 11 2022 gaoxingwang <gaoxingwang@huawei.com> - 1.32.12-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build error due to upgrading dependency packages

* Sat Dec 18 2021 gaoxingwang <gaoxingwang@huawei.com> - 1.32.12-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update NetworkManager to 1.32.12

* Wed Nov 17 2021 gaoxingwang <gaoxingwang@huawei.com> - 1.26.0-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix bluetooth,wifi,ovs,ppp module install problem

* Thu Nov 11 2021 zengweifeng <zwfeng@huawei.com> - 1.26.0-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix use after free

* Wed Oct 27 2021 gaoxingwang <gaoxingwang@huawei.com> - 1.26.0-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:decoupling bluetooth,wifi,ovs,ppp module

* Sun Oct 24 2021 gaoxingwang <gaoxingwang@huawei.com> - 1.26.0-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove rpath

* Thu Sep 2 2021 gaoxingwang <gaoxingwang@huawei.com> - 1.26.0-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:provide nm-initrd-generator to dracut

* Fri Jul 30 2021 jiazhenyuan <jiazhenyuan@uniontech.com> - 1.26.0-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix dbus-daemon error when systemd-resolved service was not enabled

* Thu Apr 29 2021 zengwefeng<zwfeng@huawei.com> - 1.26.0-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix leaking bearer in connect ready
        disconnect signals in NetworkManager device dispose
        fix wrongly considering ipv6.may fail for ipv4

* Thu Oct 29 2020 gaihuiying <gaihuiying1@huawei.com> - 1.26.0-5
- Type:requirement
- ID:NA
- SUG:NA
- DESC:don't support python2 anymore

* Thu Sep 24 2020 zhouyihang <zhouyihang3@huawei.com> - 1.26.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add attribute noreplace for NetworkManager.conf

* Fri Sep 11 2020 yuboyun <yuboyun@huawei.com> - 1.26.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix NetworkManager restarting service on dependency failure

* Thu Sep 10 2020 hanzhijun <hanzhijun1@huawei.com> - 1.26.0-2
- solve source url problem

* Fri Jul 31 2020 zhujunhao <zhujunhao8@huawei.com> - 1.26.0-1
- update to 1.26.0

* Thu Jul 30 2020 zhujunhao <zhujunhao8@huawei.com> - 1.24.2-1
- update to 1.24.2

* Thu Jul 2 2020 gaoxingwang <gxw94linux@163.com> - 1.20.10-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete a useless patch

* Fri Jun 22 2020 zhujunhao <zhujunhao8@huawei.com> - 1.20.10-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update to 1.20.10

* Mon Jun 22 2020 yanan li <liyanan032@huawei.com> - 1.16.0-8
- Fix errors when building with gtk-doc.

* Thu Feb 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix build require of GObject introspection for python

* Mon Dec 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify the patches

* Mon Oct 28 2019 caomeng <caomeng5@huawei.com> - 1.16.0-5
- Type:NA
- ID:NA
- SUG:NA
- DESC:add systemd_postun para

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-4
- Type:bugfix
- ID:NA
- SUG:NA
  DESC:the Sub-package of config-server fallback

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-3
- Type:bugfix
- ID:NA
- SUG:NA
  DESC:version fallback

* Wed Sep 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-2
- Type:bugfix
- ID:NA
- SUG:NA
  DESC:bugfix nmtui problem team module

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.16.0-1
- Package init

